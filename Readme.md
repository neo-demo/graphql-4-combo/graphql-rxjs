# **GraphQL RXJS example**

## 1. init project using nodejs

`mkdir graphql-rxjs && cd graphql-rxjs`

`npm init -y`

## 2. install dependencies

`npm i apollo-server-express graphql
rxjs apollo-server-core express 
subscriptions-transport-ws`


## 3. install devDependencies

`npm i nodemon --dev-save`

## 4. create a typeDefs
```javascript
    const typeDefs = gql`
        type Query{
            hello: String
        }
    `;
```

## 5. create resolver
```javascript
    const resolvers = {
        Query: {
            hello: () => 'Hello GraphQL'
        }
    }
```

## 4. create apollo server

```javascript
    const apolloServer = new ApolloServer({
        typeDefs,
        resolvers
    });
```
## 5. apply as a middleware

```javascript
    apolloServer.applyMiddleware({
        app, path: '/graphql'
    })
```
## 6. Express Server on port 3000
```javascript
    const PORT = 3000;
    app.listen(PORT, () => {
        console.log(`App Listening on port ${PORT}`)
    })
```
