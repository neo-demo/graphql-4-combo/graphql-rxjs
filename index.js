const express = require("express");
const {PubSub} = require("graphql-subscriptions");
const {gql} = require("apollo-server-core");
const {ApolloServer} = require("apollo-server-express");

// Create a express app
const app = express();
const typeDefs =
gql`
    type Query{
        hello: String
        getAllSubjects: [String]
    },
    type Mutation{
        addSubject(subject: String!): [String]
    }
    type Subscription{
        addSubject: String
    }
`;

const sub = ["MATHS", "PHYSICS", "CHEMISTRY", "SOCIAL SINCE", "GEOLOGY"];
const topic = 'user';
const pubSub = new PubSub();
const resolvers = {
    Query: {
        hello: () => 'Hello GraphQL',
        getAllSubjects: () => sub,
    },
    Mutation: {
        // empty parameter is for root or parent
        // 2nd parameter is argument or input
        // 3rd can be context
        // 4th will be resolver info
        addSubject: (_, {subject}) => {
            sub.push(subject);
            pubSub.publish(topic, { data: subject}).then();
            // console.log(pubSub);
            return sub;
        }
    },
    Subscription:{
        addSubject: {
            subscribe: () => {
                return pubSub.asyncIterator(topic)
            }
        }
    }
}
const apolloServer = new ApolloServer({
    typeDefs,
    resolvers
});
apolloServer.applyMiddleware({
    app, path: '/graphql'
})
const PORT = 3000;
const httpServer = app.listen(PORT, () => {
    console.log(`App Listening on port ${PORT}`)
})
apolloServer.installSubscriptionHandlers( httpServer )

